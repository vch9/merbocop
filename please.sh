#! /bin/sh
set -e

eval $(opam env)

say () { printf "mk-merbocop: $@\n" >&2 ; }


get_deps () {
    case "$(opam exec -- ocamlc --version)" in
        4.12.* ) : ;;
        * )
            say "We want OCaml version 4.12.x, you may try:"
            say "    opam switch create . 4.12.1\n"
            say "opam switch:"
            opam switch || echo ERROR
            say "OCaml version:"
            opam exec -- ocamlc --version || echo ERROR
            exit 2 ;;
    esac
    opam pin add -n ocamlformat 0.18.0
    opam install \
         base fmt cmdliner ezjsonm uri re ptime dune ocamlformat base64 ezcurl
}


build () {
    dune build @check
    dune build src/app/merbocop.exe
}

lint () {
    dune build @fmt --auto-promote
}


{ if [ "$1" = "" ] ; then build ; else "$@" ; fi ; }

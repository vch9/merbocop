open! Base

let dbg f = Fmt.kstr (Caml.Printf.eprintf "[merbocop-dbg:] %s\n%!") f

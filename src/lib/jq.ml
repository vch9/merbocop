open! Base
include Ezjsonm

let get_field f assoc =
  try List.Assoc.find_exn assoc f ~equal:String.equal
  with _ ->
    Fmt.kstr failwith "Jq.get_field: %S not found in %s" f
      (Ezjsonm.value_to_string (`O assoc))
